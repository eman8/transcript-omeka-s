<?php
namespace Transcript;

use Omeka\Module\AbstractModule;
use Omeka\Mvc\Exception\RuntimeException as MvcRuntimeException;

use Laminas\Form\Element;
use Laminas\Form\Form;
use Omeka\Form\Element as OmekaElement;
use Laminas\Form\Fieldset;
use Laminas\Form\Element\Textarea;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractController;
use Laminas\Mvc\MvcEvent;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class Module extends AbstractModule
{
    public function attachListeners(SharedEventManagerInterface $sharedEventManager)
    {
        $sharedEventManager->attach(
            \Omeka\Api\Adapter\MediaAdapter::class,
            'api.update.post',
              function ($event) {
                  $data = $event->getParam('request')->getContent();
                  $media = $event->getParam('request');
                  $id = $media->getId();
                  $connection = $this->getServiceLocator()->get('Omeka\Connection');
                  $sql = $connection->prepare("DELETE FROM value WHERE resource_id = $id AND property_id = 83");
                  $delete = $sql->execute();
                  $sql = $connection->prepare("INSERT INTO value VALUES (null, $id, 83, null, 'literal', '', " . $connection->quote($data['transcription']) . ", null, 1)");
                  $insert = $sql->execute();
              }
        );

        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Media',
            'view.edit.form.after',
              function (Event $event) {
                $form = $event->getParam('form');
                $renderer = $event->getTarget();
                $media = $renderer->vars()->media;
                $connection = $this->getServiceLocator()->get('Omeka\Connection');
                $transcription = $connection->executeQuery("SELECT value FROM value WHERE resource_id = " . $media->id() . " AND property_id = 83")->fetchFirstColumn();
                $element = new \Laminas\Form\Element\Textarea('transcription');
                $element->setAttribute('id', 'transcription');
                if ($transcription) {
                  $element->setValue($transcription[0]);
                }
                $resourceType = $form->getOption('resource_type');
                echo $event->getTarget()->partial('transcript/index/form', [
                    'element' => $element,
                    'id' => 'transcription',
                    'media' => $media,
                ]);
              }
        );

        /*
         * Add an "Transcribe Media" tab to the media show page.
         */
        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Media',
            'view.edit.section_nav',
            function (Event $event) {
                $view = $event->getTarget();
                $sectionNavs = $event->getParam('section_nav');
                $sectionNavs['transcript'] = 'Transcribe this media';
                $event->setParam('section_nav', $sectionNavs);
            }
        );
    }

/*
    public function addTranscriptForm($event)
    {
    $services = $this->getServiceLocator();
        $api = $services->get('Omeka\ApiManager');
        $view = $event->getTarget();

        $resource = $view->resource;
        $form = $event->getParam('form');

    }
*/

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get this module's configuration form.
     *
     * @param ViewModel $view
     * @return string
     */
    public function getConfigForm(PhpRenderer $renderer)
    {
        return '<input name="foo">';
    }

    /**
     * Handle this module's configuration form.
     *
     * @param AbstractController $controller
     * @return bool False if there was an error during handling
     */
    public function handleConfigForm(AbstractController $controller)
    {
        return true;
    }
}
