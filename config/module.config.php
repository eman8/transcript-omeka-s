<?php
return [
    'view_manager' => [
        'template_path_stack' => [
            OMEKA_PATH . '/modules/Transcript/view',
        ],
    ],
    'controllers' => [
        'invokables' => [
            'Transcript\Controller\Index' => 'Transcript\Controller\IndexController',
        ],
    ],
    'form_elements' => [
      'factories' => [
          'Transcript\Form\ConfigForm' => 'Transcript\Service\Form\ConfigFormFactory',
      ],
    ],
/*
     'navigation' => [
          'AdminModule' => [
              [
                  'label' => 'Transcript',
                  'route' => 'admin/transcript',
                  'resource' => 'Transcript\Controller\Index',
                  'pages' => [
                      [
                          'label' => 'List',
                          'route' => 'admin/transcript',
                          'action' => 'list',
                          'resource' => 'Transcript\Controller\Index',
                      ],
                  ]
              ]
          ]
      ],
      'router' => [
          'routes' => [
              'admin' => [
                  'child_routes' => [
                      'transcript' => [
                          'type' => 'Literal',
                          'options' => [
                              'route' => '/transcript',
                              'defaults' => [
                                  '__NAMESPACE__' => 'Transcript\Controller',
                                  'controller' => 'Index',
                                  'action' => 'list',
                              ],
                          ],
                          'may_terminate' => true,
                    ],
              ],
          ],
      ],
    ]
*/
];
