# Transcript for Omeka-S

This module adds the ability to transcribe medias in TEI format with a WYSIWYG editor.

## Install 

Install the module as usual.

## Usage

The module adds a tab "Transcribe this media" on the media edit page.

Here you will find a rendering of the media on the left, and a WYSIWYG text editor on the right.

The dropdown menu at the top of the editor allows to add tags to the current selection or at the selected place.

The menu is contextual, which means the tags you are allowed to use depend on the place the cursor is in the text, or what the current selection is.

Some tags will pop up a dialog box allowing you to type or choose the values of the tag's attributes.

## Credits

La version actuelle a été réalisée avec le soutien du projet Collex Amor (Université Paris-Saclay).



