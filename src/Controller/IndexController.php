<?php
namespace Transcript\Controller;

use Omeka\Form\ConfirmForm;
use Omeka\Stdlib\Message;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct()
    {
    }

    public function listAction()
    {
        $view = new ViewModel;
//         $this->logger()->debug("listAction");
        // Deduce constraints from XML schema
        $xmlSchema = simplexml_load_file(OMEKA_PATH . '/modules/Transcript/asset/resources/cm-tei-schema.xml');
        $constraints = [];
        foreach ($xmlSchema as $tag => $members) {
          foreach ($members->children as $i => $childTag) {
            $childTag = (string) $childTag;
            ! isset($constraints[$childTag]) ? $constraints[$childTag] = [] : null;
            $constraints[$childTag][] = $tag;
          }
        }
        $controles = '{';
        foreach ($constraints as $name => $tag) {
          $controles .= '"' . $name . '":["' . implode('","', $tag) . '"],';
        }
        // Special case for paragraph alignment
        $allowed = '["p","cell","div","foreign","item","sp"]';
        $controles = substr($controles, 0, -1) . ',"pleft":' . $allowed . ', "pright":' . $allowed . ', "pcenter":' . $allowed . ', "pjustify":' . $allowed . '}';
        $view->setVariable('controles', $controles);
        $view->setVariable('transcriptions', 'Transcriptions list will be here');
        $view->setVariable('comments', ' ');
        $view->setVariable('WEB_ROOT', __DIR__);
        return $view;
    }

    public function saveTranscriptionAction()
    {
        $post = 'NON';
        if (! empty($_POST)) {
          $post = 'OUI';
        }
        $view = $event->getTarget();
        $view->setVariable('post', $post);
    }

/*
    public function optionsAction()
    {
        $view = new ViewModel;
        $view->setVariable('transcriptions', 'Transcriptions list');
        return $view;
    }
*/

}
